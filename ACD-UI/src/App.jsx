import * as React from 'react';
import {Button,Container,Row,Col,Navbar,Nav} from 'react-bootstrap';
import './App.css';
import SpreadsheetSN from './SpreadsheetSN.jsx';
import axios from 'axios';
import Main from './Main';
import POCDocument from './POCDocument';
import { BrowserRouter, Route, Switch } from 'react-router-dom';

 class App extends React.Component { 
   
    
     render() {
     
        return  (<div className="Main-body-wrapper">
            <div className="side-navigation">
                <ul className="menu">
                    <li className="menu_items toggle_menu">
                    <button type="button" class="btn"><i class="fas fa-bars"></i></button>
                    </li>
                    <li className="menu_items ">
                    <button type="button" class="btn "><i class="fas fa-tachometer-alt"></i>
                    
                    </button>
                    </li>
                    <li className="menu_items ">
                    <a href="POCDocument"> <button type="button" class="btn"><i class="fas fa-list-alt"></i>
                    
                    </button></a>
                    </li>
                    <li className="menu_items ">
                    <a href="Main"> <button type="button" class="btn"><i class="fas fa-chart-line"></i>
                    
                    </button></a>
                    </li>
                </ul>
            </div>
         <div className=" d-flex flex-grow-1">

<BrowserRouter>
<Switch>
<Route path="/Main">
<Main />
</Route> 
<Route path="/POCDocument">
<POCDocument />
</Route>

</Switch>
</BrowserRouter>


</div> 
  
          {/* <Main></Main> */}
             {/* <POCDocument></POCDocument>   */}
      </div>
      
        );
    }
}
export default App;