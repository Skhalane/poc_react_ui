import * as React from 'react';
import {Button,Container,Row,Col,Navbar,Nav} from 'react-bootstrap';
import './App.css';
import SpreadsheetSN from './SpreadsheetSN.jsx';
import axios from 'axios';

 class Main extends React.Component { 
    state={
        fileList:[],
        spData:[],
        fName:"",
      
        };
        constructor(props) {
            super(props);
           this.loadDocumentdata = this.loadDocumentdata.bind(this);

          }    
       
          handleCallback = (childData) =>{
            this.setState({fileList: childData})
        }   

    componentDidMount()
    {
       // debugger;
        axios.get('http://localhost:28358/api/spreadsheets').then(res=>{
            // console.log(res);
            this.loadDocumentdata(res.data[0].fileName);
            this.setState({fileList:res.data});
        });
    }  
    toggleSpreadSheetMenu=()=>{
        this.setState({isSpredsheetMenuOpen: !this.state.isSpredsheetMenuOpen})
    }  
    loadDocumentdata(fileName) {
        //  alert(fileName);
        debugger;
        this.state.fName=fileName;
        axios.get("http://localhost:28358/api/spreadsheets/getfilebyname/" + fileName, { responsetype: 'json' }).then(res=>{
            // var abc=res;
            // console.log(abc.data);
         
        
            // this.spreadsheet.sheets[0].ranges[0].dataSource = [];
            this.setState({spData:res.data});
            })
      }

      loadVersionFile(fileDetail,filename){
        //   alert(fileDetail +filename); 
        // filename= this.state.fName;
        this.selectedFileName=fileDetail;
        axios.get("http://localhost:28358/Excels/" + "G703" +"/"+filename, { responseType: 'json' }).then((res) => {
        //   var editorDiv = document.querySelector("#froalaDiv .fr-element");
        //   editorDiv.innerHTML = ""; 
        //   editorDiv.innerHTML = htmlData;
        // console.log(res);
        //  alert(res);
        // alert("hi");
        
         this.setState({spData:res.data});//
        //  this.setState({spData:res.data.data});//
        // this.state.varsionFileData=res.data;
        });
      }
      
     
     render() {
     
        return  (<div className="flex-grow-1">
        <>
        <Container fluid className={"Main-body-container"}>
            <Row className={"Main-header"}>
                <Col sm={12} >
                <Navbar  >
                    <Navbar.Brand class="text-center" >AIA® Document</Navbar.Brand>
                    <div className="right-side-nav">
                        <div className="menu-item-group">
                        <i class="far fa-bell menu-item"></i>
                        </div>
                    <div className="user_profile">
                        <div className="user_avtar">JD</div>
                        <i class="fas fa-sort-down"></i>
                    </div>
                    </div>
                    {/* <Navbar.Brand class="text-center">
                    <img src={require('./assets/img/MainLogo.svg')} />
                    </Navbar.Brand> */}
                </Navbar>
                
                </Col>
            </Row>
           
            <Row className={"Main-body"}>


              <button className="spreadSheetToggleBtn btn" onClick={this.toggleSpreadSheetMenu} type="button" >
       {(!this.state.isSpredsheetMenuOpen) &&
           <i class="fas fa-arrow-circle-right"></i>
       }
       {(this.state.isSpredsheetMenuOpen) &&
           <i class="fas fa-arrow-circle-left"></i>
       }
           </button>
{/* <Col sm={2} className={"bg-light"} > */}
                {
                (this.state.isSpredsheetMenuOpen) &&
                <div class="row bg-light spread_sheet_menu"   >
                    <Row >
                            {
                            this.state.fileList.map(({fileName, version}, i) => (
                                <Col sm={12}>
                                <div key={i}>
                                <div>
                                <div  className="link_menu_wrapper">
                                {/* <span onClick={() => this.loadDocumentdata(fileName)}  >{fileName}</span> */}
                                <span className="SpreadSheet_name" onClick={() => this.loadDocumentdata(fileName)}  ><span className="title-icon spread_sheet_icon"><i class="far fa-file-alt"></i></span>{fileName.replace(".xlsx","")}</span>
                                   {version.map((vfile, j) => <div className="json_name" onClick={() => this.loadVersionFile(fileName,vfile)}   key={j}>
                                   <span className="user_initials user_color1" >SV</span>
                                      <span className="file_name">{vfile.replace(".json","")}</span>
                                      <span className="time_stamp">6/10/2021 10:23am</span>        
                                       </div>)}                                
                                   </div>
                                </div>
                            
                                </div>
                                </Col>
                                ))
                            } 
                 


                       
                      
                    </Row>
                    </div>
            }
               
               
               
               
               
               
                  
                <div class="col-lg-12" id="spreadSheeteditor_container_body">   
                    <div className="doc_project_title"><strong>Project:</strong> Brookwood High school, Snellville, GA | A101 2007</div>

                <SpreadsheetSN className="spreadsheet_container" spDatas={this.state.spData} sfileName={this.state.fName} parentCallback = {this.handleCallback}  ></SpreadsheetSN> 
                </div >
            </Row>


           <Row className={"Main-footer"}>
           © 2021 AIA® Document. All rights reserved 
           </Row >
            </Container>
            
      </>
    
      </div>
      
        );
    }
}
export default Main;