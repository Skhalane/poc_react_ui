import * as React from 'react';
import axios from 'axios';
import {Button,Container,Row,Col,Navbar} from 'react-bootstrap';
import {getRangeAddress, isLocked } from '@syncfusion/ej2-spreadsheet';
import { SpreadsheetComponent, SheetsDirective, SheetDirective,RangesDirective , RowsDirective, CellsDirective, RowDirective, CellDirective } from '@syncfusion/ej2-react-spreadsheet';
import { RangeDirective, ColumnsDirective, ColumnDirective } from '@syncfusion/ej2-react-spreadsheet';
// import { DialogComponent } from '@syncfusion/ej2-react-popups';
import './App.css';
 class SpreadsheetSN extends React.Component {

        constructor(...props) {
        super(props);

            this.state={
                value: props.initialValue,
                dataset2:this.props.spDatas ,
                AppNo:"",
                
            };
        let isCreated = false;
            this.response=[];
            console.log(this.props.spDatas , "text");
            this.response=this.props.spDatas; 
        this.image1 = [{ src: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAggAAACACAYAAABugJH/AAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwACRVMAAkVTAXmnuGkAAB/cSURBVHhe7ZRdkiwr0u16/qPqmd3b1Kd1LE6VnIzIXyCRmZ6QU3jktv2fzWaz2Ww2m81ms9lsNpvNZrPZbDabzWaz2Ww2m1fz3//+9//NLqt8JfY9VpWVN0/CvvG9cuWS2L7bc/IJFeu3r5PPfg67YGVZeylsz2+Uz7E5iX3DZ8j1y2A7bq/L5/wHa7bvkZ/gNjb8bfIppsT22f6ffKJNgX2zZ8j1y2A7bq/L5/wHa7bvkZ/gNjb8zfJZpsF22Nby2Tb/w77Ps+RPLIHtt71PPukPdr59n/wMfWxwO89/cPb27W35fF+LfZNnyp9ZAttve5980h/sfPs++Rn62OD23/KphsTeuz0vn/HrsG/xTPkzS2D7be+TT/qDnW/fJz9DHxvcunyyobB3NjleDtv1Ubn6q7Dv8Gz5U9Nju23vk0/6g51v3yc/Qx8bjCTTYDs8W/7UMNgbmxx/DfYNrspVy2O7R5LT2B2RZHpst0iyOWDfKZL8YOeRZPMg9m0jSR8bjCRLYXtelauGwN7X5PhrsW9yRsaXxvaOJKexO46STY3tFUk2B+w7RZIf7DySbB7Evm0k6WODkWR5bPdbMvpx7G1NjjcH7DuZ5MtiO0eSS9g9kWRqbK9Isjlg3ymS/GDnkWTzIPZtI0kfG4wkX4V9h0pGPoq9q8nxRrDv9VvS5bBdI8ll7K6jZNNiO0WSzQH7TpHkBzuPJJsHsW8bSfrYYCT5Sux7VDLyEew9TY43BfbNfku6FLZnJLkLu+8o2ZTYPpFkc8C+UyT5wc4jyeZB7NtGkj42GEm+FvsmlYy8HXtLk+PNDezbHSVbAtsvkihnumNjkk2J7RNJNgfsO0WSH+w8kmwexL5tJOljg5Gki829Uv7s27A3VDLyVuwdTY5PYfMjy7Ofhv2No2TTY7tFkn9hXZPjP1gbSabE9okkN7HZb5TP8YOdjyjPPY3dMaI8t48NRhLF+tHkqQ9jd1cy8jbsDU2Ou9jczLLWXdh9R8mmxvaKJP/CuibHf7D2KNl02C6RpMRmvlk+yw92Pqo8uYvNjSzP7mODkUSxfnR5+l3YfZWMvAX7+02Ou9jcKrLiJeyeSDIttlMk+YO1keQP1kaS6bBdIkmJzXyzfJYf7HxUeXIXmxtZnt3HBiOJYv1MssZl7C6T/OXY325y3MXmVpNVT2N3RJIpsX0iyR+sjSR/sDaSTIftEklKbOab5bP8YOcjy7NLbGZkeXYfG4wkivWzykqnsTtM8pdif7fJcRebW1HWPY3dEUmmw3aJJP/CuqNkivWRZCpsj0hSYjPfKp/kH6wZWZ5dYjOjypNvY8ORRLF+dlntFDb/W9KXYn+3yXEXm1tVVj6FzUeSqbA9IskfrP0t6R+sjSRTYXtEkhKb+Vb5JP/CulHlySU2M6o8+TY2HEkU61eQ9U5h878lfRn2N5scd7G5lWXtU9h8JJkG2yGS/MHa35L+wdpIMhW2RyQpsZlvk0/RxeZGk6eW2Mxo8tTz2CWRRLE+krwE+3vPlj91Cpv/LelLsL/X5LiLzUWSIbH3npUrTmHzTY6nwXaIJH+w9rekivWRZBpsh0hSYjORZDMQ9jtFkhKbiSTzYctEEsX6SPJx7G1n5YpT2PxvSZ+O/a0mx11sLpJMhe1hkt/EZiPJ8NjbI4li/W9JFesjyTTYDpGkxGYiyWYg7HeKJCU2E0nmw5aJJIr1kWQ47K09GTuFzR8lezr2t5ocd7G5SDIdtstvSU9h802Oh8feHkn+YG0lI3+wNpJMg+0QSUpsJpJsBsJ+p0hSYjORZD5smUiiWB9JhsbebZLfxGaPkj0d+1tNjrvYXCSZEtvnt6Q3sdkmx8Njb48kf7C2khHF+kgyBfb+SFJiM5FkMxD2O0WSEpuJJPNhy0QSxfpIMjz2dpP8JjZ7lOyp2N9pctzF5iLJtNhOR8luYrORZFjszZFEsb6SEcX6SDIF9v5IUmIzkWQzEPY7RZISm4kk82HLRBLF+kgyBfZ+k/wmNhtJnor9nSbHXWwukkyN7XWU7CY22+R4WOzNkUSxvidjf7A2kkyBvT+SlNhMJNkMhP1OkaTEZiLJfNgykUSxPpJMg+1gkt/EZiPJ07C/0eS4i81Fkumx3Y6SdbG5JsfDYm+OJH+w9paMKtZHkuGxt0eSEpuJJJuBsN8pkpTYTCSZD1smkijWR5KpsD1M8i42F0mehv2NJsddbC6STI/tdpSsi801OR4Se28kUay/JaOK9ZFkeOztkaTEZiLJZiDsd4okJTYTSebDlokkivWRZDpsF5O8xGYiydOwv9HkuIvNRZIlsP0iSReba3I8JPbeSPIHa+OZ8wrrI8nQ2LsjSYnNRJLNQNjvFElKbCaSzIctE0kU6yPJlNg+vyXtYnOR5CnY/U2Ou9hcJFkC2+8oWYnNNDkeDntrJFGsj2fOK6yPJENj744kJTYTSTYDYb9TJCmxmUgyH7ZMJFGsjyTTYjv9lrTEZiLJU7D7mxx3sblIsgy2YyQpsZkmx8Nhb40kivWR5K67rT1KNiz25khSYjORZDMQ9jtFkhKbiSTzYctEEsX6SDI1ttdRshKbiSRPwe5vctzF5iLJMtiOkaTEZpocD4W98yjZH6yNJD/YeSRRrI8kw2JvjiQlNhNJNgNhv1MkKbGZSDIftkwkUayPJNNjux0lK7GZSPIwdneT4y42F0mWwXaMJCU2E0mGwd4YSRTrI8kPdh5JFOuPkg2JvTeSlNhMJNkMhP1OkaTEZiLJfNgykUSxPpIsge0XSUpsJpI8jN3d5LiLzUWSZbAdI0kXm2tyPAT2vqNkivWR5B+siSSK9ZFkSOy9kaTEZiLJZiDsd4okJTYTSebDlokkivWRZAlsv6NkJTYTSR7C7m1y3MXmIsky2I6RpIvNNTkeAntfJCmxmUjyD9ZEEsX6o2TDYW+NJCU2E0k2A2G/UyQpsZlIMh+2TCRRrI8ky2A7RpISm4kkD2H3NjnuYnORZBlsx0jSxeaaHH8ce9tRMsX6SPIHayOJYn0kGQ57ayQpsZlIshkI+50iSYnNRJL5sGUiiWJ9JFkG2zGSlNhMJHkIu7fJcRebiyRLYPtFkpvYbJPjj2NviyQlNhNJ/mBtJFGsjyTDYW+NJCU2E0k2A2G/UyQpsZlIMh+2TCRRrI8kS2F7RpISm4kkd2N3NjnuYnORZAlsv0hyE5ttcvxx7G2RRLH+KNkfrI0kJTYTSYbC3hlJSmwmkmwGwn6nSFJiM5FkPmyZSKJYH0mWwvaMJCU2E0nuxu5sctzF5iLJEth+kaSLzUWSj2LviiQlNhNJSmwmkijWR5KhsHdGkhKbiSSbgbDfKZKU2EwkmQ9bJpIo1keSpbA9I0mJzUSSu7E7mxx3sblIsgS2XyTpYnNNjj+OvS2SKNYfJSuxmUiiWB9JhsLeGUlKbCaSbAbCfqdIUmIzkWQ+bJlIolgfSZbC9owkXWwuktyF3dfkuIvNRZLpsd0iyU1stsnxx7G3RRLF+kjSxeYiSYnNRJJhsDdGkhKbiSSbgbDfKZKU2EwkmQ9bJpIo1keS5bBdI0mJzUSSu7D7mhx3sblIMj22WyS5ic02Of4o9q5IUmIzkeQmNhtJFOsjyTDYGyNJic1Eks1A2O8USUpsJpLMhy0TSRTrI8ly2K6RpMRmIsld2H1NjrvYXCSZGtsrkpzC5pscfxR7VyRRrI8kp7D5SKJYH0mGwd4YSUpsJpJsBsJ+p0hSYjORZD5smUiiWB9JlsN2jSQlNhNJ7sLua3LcxeYiydTYXpHkJjYbST6KvSuSKNZHktPYHZFEsT6SDIG9L5KU2Ewk2QyE/U6RpMRmIsl82DKRRLE+kiyH7RpJSmwmktyF3dfkuIvNRZIpsX1+S3oTm21y/FHsXZFEsT6SXMLuiSSK9ZFkCOx9kaTEZiLJZiDsd4okJTYTSebDlokkivWRZDls10jSxeYiyWXsribHXWwukkyH7WKS38Rmmxx/FHtXJFGsjySXsHsiSYnNRJKPY2+LJCU2E0k2A2G/UyQpsZlIMh+2TCRRrI8kS2L7RpISm4kkl7G7mhx3sblIMhW2h0l+E5uNJB/D3hRJSmwmklzG7ookivWR5OPY2yJJic1Eks1A2O8USUpsJpLMhy0TSRTrI8mS2L6RpMRmIsll7K4mx11sLpJMgb2/J2M3sdkmxx/F3hVJSmwmklzG7ookivVHyT6KvSuSlNhMJNkMhP1OkaTEZiLJfNgykUSxPpIsie0bSUpsJpJcxu5qctzF5iLJsNibz8j4TWw2knwUe1ckUayPJHdh90WSEpuJJB/F3hVJSmxmNVl1CWy/SFJiM5FkPmyZSKJYH0mWxPaNJCU2E0kuY3c1Oe5ic5FkGOyNV+SaU9h8JPko9q5IUmIzkeQu7L5IUmIzR8k+hr0pkpTYzIqy7vTYbpGkxGYiyXzYMpFEsT6SLIntG0m62FwkuYTd0+S4i82tKOuexu6IJB/F3hVJSmwmktyN3RlJSmwmknwMe1MkKbGZFWXd6bHdIkmJzUSS+bBlIolifSRZEts3knSxuUhyCbunyXEXm1tNVj2N3RFJPo69LZIo1h8luxu7M5KU2MxRso9g74kkJTazqqw8NbZXJCmxmUgyH7ZMJFGsjyRLYvtGki42F0kuYfc0Oe5ic6vIipeweyLJx7G3RZISm4kkD2H3HiUrsZlI8hHsPZGkxGZWlZWnxvaKJCU2E0nmw5aJJIr1kWRJbN9I0sXmIskl7J4mx11sbgVZ7zR2x29JP469LZIo1h8lexi7O5KU2Ewk+Qj2nkjSxeZWlHWnxvaKJCU2E0nmw5aJJIr1kWRJbN9I0sXmIskl7J4mx11sbmZZ6xJ2z29Jh8DeF0kU6yPJU7D7I0kXm4skb8feEklOYfMryZpTY3tFkhKbiSTzYctEEsX6SLIktm8k6WJzkeQSdk+T45vY7Azy/Iewe39LOgT2vkhSYjOR5GnY34gkJTYTSd6OvSWSfA32DSLJ1NhekaTEZiLJfNgykUSxPpIsie0bSbrYXCS5hN3T5Pg0dsdI8synYPeb5MNgb4wkivWR5KnY34kkXWwukrwVe0ck+RrsG0SSqbG9IkmJzUSS+bBlIolifSRZEts3knSxuUhyCbunyfHmF/atTPJhsDdGkhKbiSRPx/5WJCmxmUjyVuwdkeRrsG8QSabG9ookJTYTSebDlokkivWRZFls50hSYjOR5BJ2T5PjDdg3MsmHw94aSRTrI8lLsL8XSUpsJpK8FXtHJPka7BtEkqmxvSJJic1EkvmwZSKJYn0kWRbbOZKU2EwkuYTd0+T467FvU8nIkNh7I4lifSR5Cfb3IkkXm4skb8PeEEm+BvsGkWRqbK9IUmIzkWQ+bJlIolgfSZbFdo4kJTYTSS5h9zQ5/hrsG1yRa4bE3htJSmwmkrwM+5uRpMRmIsnbsDdEkq/BvkEkmRrbK5KU2EwkmQ9bJpIo1keSZbGdI0mJzUSSS9g9TY6Xxva+KlcNjb07kijWR5KXYn83kpTYTCR5G/aGSPI12DeIJFNje0WSEpuJJPNhy0QSxfpIsiy2cyQpsZlIcgm7p8nxctiu98qVw2NvjySK9ZHk5djfjiQlNhNJ3oL9/UhyE5tdTVadGtsrkpTYTCSZD1smkijWR5IlsX0jSRebiySXsHuaHJ/C5l8lf/Iu7L575LopsPdHEsX6SPIW7O9HkhKbiSRvwf5+JCmxmVVl5amxvSJJic1EkvmwZSKJYn0kWRLbN5J0sblIcgm7p8nxTWz2XfKEU9j8VblqKmyPSKJYH0negv39SFJiM0fJXo797UhSYjOryspTY3tFkhKbiSTzYctEEsX6SLIktm8k6WJzkeQSdk+T4y429wl5ThebOytXTIftcpRMsT6SvA17QyQpsZlI8nLsb0eSEptZVVaeGtsrkpTYTCSZD1smkijWR5IlsX0jSYnNRJLL2F1NjrvY3CflWSU2U8nI1NhekaTEZiLJ27A3RJISmzlK9lLs70aSEptZUdadHtstkpTYTCSZD1smkijWR5IlsX0jSYnNRJLL2F1NjrvY3AjyvD9Y25OxKbF9jpIp1o8qT+5ic5HkpdjfjSQlNrOirDs9tlskKbGZSDIftkwkUayPJEti+0aSEpuJJJexu5ocd7G5UeSJivWVjEyH7RJJSmxmVHlyF5s7SvYy7G9GkhKbWUnWXAbbMZKU2EwkmQ9bJpIo1keSJbF9I0mJzUSSy9hdTY672FyT46djf6snY11s7rek02A7HCUrsZmR5dldbC6SvAz7m5GkxGYiyWYg7HeKJCU2E0nmw5aJJIr1kWRJbN9IUmIzkeQydleT4y421+T4pdjfNclvYrNHyabA3h9JutjcyPLsLjYXSV6G/c1IUmIzkWQzEPY7RZISm4kk82HLRBLF+kiyJLZvJCmxmUhyGburyXEXm2ty/Bbs7/+W9CY2e5RseOztkaSLzY0qTz6FzUeSl2B/L5KU2Ewk2QyE/U6RpMRmIsl82DKRRLE+kiyJ7RtJSmwmklzG7mpy3MXmmhy/FXvHUbKb2OxRsmGxN0eSU9j8iPLcU9h8JHkJ9vciSYnNRJLNQNjvFElKbCaSzIctE0kU6yPJctiukaSLzUWSy9hdTY672FyT47djbzlKdhObPUo2JPbeSHIJu2cEed5l7K5I8nTsb0WSEpuJJJuBsN8pkpTYTCSZD1smkijWR5LlsF0jSRebiySXsbuaHHexuSbHH8Hec5TsJjZ7lGwo7J2R5OuxbxNJno79rUhSYjORZDMQ9jtFkhKbiSTzYctEEsX6SLIctmsk6WJzkeQydleT4y421+T4Y9ibIskpbP4o2TDYGyPJ12PfJpI8HftbkaTEZiLJZiDsd4okJTYTSebDlokkivWRZDls10hSYjOR5C7svibHXWyuyfFHsXdFklPY/FGyIbD3RZLN/7DvE0meiv2dSFJiM5FkMxD2O0WSEpuJJPNhy0QSxfpIshS2ZyTpYnOR5C7svibHXWyuyfHHsbdFklPY/FGyj2LviiQbsG8USZ6K/Z1IUmIzkWQzEPY7RZISm4kk82HLRBLF+kiyFLZnJOlic5HkLuy+JsddbK7J8cext0WS09gdR8k+hr0pkmzAvlEkeSr2dyJJic1Eks1A2O8USUpsJpLMhy0TSRTrI8lS2J6RpIvNRZK7sPuaHHexuSbHQ2DviySnsTuOkn0Ee08k6WJzM8o6N7HZSPI07G9EkhKbiSSbgbDfKZKU2EwkmQ9bJpIo1keSZbAdj5KV2EwkuRu7s8lxF5trcjwM9sZIchq7I5K8HXtLJCmxmdlltS42F0mehv2NSFJiM5FkMxD2O0WSEpuJJPNhy0QSxfpIsgy2YyTpYnOR5G7szibHXWyuyfEw2BuPkp3G7ogkb8XeEUlKbGZ2Wa2LzUWSp2F/I5KU2Ewk2QyE/U6RpMRmIsl82DKRRLE+kiyD7RhJuthcJLkbu7PJcReba3I8FPbOSHIJu6fJ8Vuxd0SSEptZQdbrYnOR5CnY/ZGkxGYiyWYg7HeKJCU2E0nmw5aJJIr1kWQJbL9I0sXmIslD2L1NjrvYXJPj4bC3RpLT2B2R5C3Y348kJTaziqzYxeYiyVOw+yNJic1Eks1A2O8USUpsJpLMhy0TSRTrI8kS2H6RpIvNRZKHsHubHHexuSbHQ2LvjSSnsTuaHL8F+/uRpMRmVpI1S2wmkjwFuz+SlNhMJNkMhP1OkaTEZiLJfNgykUSxPpJMj+0WSbrYXCR5GLu7yXEXm2tyPCT23qNkp7D5SPJy7G9HkhKbWUnW7GJzkeRh7O5IUmIzkWQzEPY7RZISm4kk82HLRBLF+kgyPbZbJOlic5HkYezuJsddbK7J8bDYmyPJaeyOSPIy7G9Gki42F0mGx94eSbrYXCR5GLs7kpTYTCTZDIT9TpGkxGYiyXzYMpFEsT6STI3tdZSsi81Fkoexu5scd7G5JsdDY++OJKew+aNkL8H+XiQpsZlIMg22QyQpsZmjZA9h90aSEpuJJJuBsN8pkpTYTCSZD1smkijWR5Kpsb0iSRebiyRPwe5vctzF5pocD4+9vcnxaeyOSPIS7O9FkhKbiSTTYDtEki42F0kewu6NJCU2E0k2A2G/UyQpsZlIMh+2TCRRrI8k02I7HSXrYnOR5CnY/U2Ou9hck+PhsbdHktPYHZHkqdjfiSQlNhNJpsN2iSQlNnOU7G7szkhSYjORZDMQ9jtFkhKbiSTzYctEEsX6SDIlts9Rsi42F0mehv2NJsddbK7J8RTY+yPJaeyOJsdPxf5OJCmxmUgyHbZLJOlic0fJ7sLuiyQlNhNJNgNhv1MkKbGZSDIftkwkUayPJNNhu/yWtIvNRZKnYX+jyXEXm2tyPAX2/khyGrsjkjwFuz+SdLG5SDIdtksk6WJzR8nuwu6LJCU2E0k2A2G/UyQpsZlIMh+2TCRRrI8kU2F7/Ja0i80dJXsa9jeaHHexuSbH02A7RJLT2B2R5GHs7khSYjORZFpsp0jSxeYiyV3YfZGkxGYiyWYg7HeKJCU2E0nmw5aJJIr1kWQabAeTvIvNRZKnYn+nyXEXm2tyPBW2RyQ5hc1HkoexuyNJic1EkmmxnSJJF5s7SnYZuyuSlNhMJNkMhP1OkaTEZiLJfNgykUSxPpIMj729kpEuNneU7KnY32ly3MXmmhxPhe1xlOwUNh9J7sbujCQlNhNJpsd2iyRdbC6SXMbuiiQlNhNJNgNhv1MkKbGZSDIftkwkUayPJENj765kpIvNHSV7Ova3mhx3sbkmx9Nhu0SS09gdTY7vxu6MJCU2E0mmx3aLJF1sLpJcxu6KJCU2E0k2A2G/UyQpsZlIMh+2TCRRrI8kQ2Lv7clYF5v7LenTsb/V5LiLzTU5nhLbp8nxaeyOSHIXdl8kKbGZSDI9tlskuYnNRpJL2D2RpMRmIslmIOx3iiQlNhNJ5sOWiSSK9ZFkKOydt2S0i839lvQl2N9rctzF5pocT4ntE0lOY3c0Ob6M3RVJSmwmkiyD7RhJuthcJLmE3RNJSmwmkmwGwn6nSFJiM5FkPmyZSKJYH0k+ir3rilxzE5v9LelLsL/X5LiLzTU5nhbbqcnxaeyOSHIJuyeSlNhMJFkG2zGSdLG5SHIJuyeSlNhMJNkMhP1OkaTEZiLJfNgykUSxfgVZ7xQ2/1vSl2F/s8lxF5trcjwttlMkOY3dEUlOY3dEEsX6SLIctmsk6WJzkeQ0dkckKbGZSLIZCPudIkmJzUSS+bBlIoli/eyy2ils/rekL8X+bpPjLjbX5HhqbK9IcgqbP0p2E5uNJCU2E0mWw3aNJF1sLpKcxu6IJCU2E0k2A2G/UyQpsZlIMh+2TCRRrJ9VVjqN3fFb0pdjf7vJcReba3I8NbbXUbJT2HwkuYnNRpISm4kky2G7RpKb2GwkOYXNR5ISm4kkm4Gw3ymSlNhMJJkPWyaSKNbPJqtcwu75LelbsL/f5LiLzTU5nh7bLZKcxu6IJCU2E0lKbCaSLIvtHEm62FwkOYXNR5ISm4kkm4Gw3ymSlNhMJJkPWyaSKNbPIM+/C7vPJH8L9vebHHexuSbHS2D7RZLT2B2RRLE+kpTYTCRZFts5knSxuUhyCpuPJCU2E0k2A2G/UyQpsZlIMh+2TCQpsZlR5cl3Y3ea5G/D3tDkuIvNNTleBtsxkpzC5o+S/QvrjpKV2EwkWRbbOZLcxGYjyU1sNpKU2Ewk2QyE/U6RpMRmIsl82DKR5CY2+2l52sPY3ZWMvBV7R5PjLjbX5HgZbMejZKew+UjyL6yLJF1sLpIsje0dSbrYXCS5ic1GkhKbiSSbgbDfKZKU2EwkmQ9bJpJ8LfZNKhl5O/aWJsddbK7J8VLYnpHkNHZHJPnBzo+SldhMJFke2z2SdLG5SHITm40kJTYTSTYDYb9TJCmxmUgyH7ZMJPk67Fv0ZOwj2HuaHHexuSbHy2G7RpLT2B1Njn+w80jSxeYiyfLY7pHkJjYbSbrYXCQpsZlIshkI+50iSYnNRJL5sGUiyVdh36EnYx/D3tTkuIvNNTleEtu3yfFp7I5467z5c0kHm4kkX4N9g0jSxeYiSRebiyQlNhNJNgNhv1MkKbGZSDIftkwk+Qps/1sy+lHsXU2Ou9hck+MlsX0jyWnsjjMy3sXmIsnXYN8gknSxuaNkJTYTSUpsJpJsBsJ+p0hSYjORZD5smUiyJLbvWbliCOx9TY672FyT42WxnZscn8buOCPjXWwuknwN9g0iyU1sNpKU2EwkKbGZSLIZCPudIkmJzUSS+bBlIskS2H73yHXDYG9sctzF5pocL4vtHElOY3f0ZKyLzUWSr8O+RSTpYnNHyRTrI0mJzUSSzUDY7xRJSmwmksyHLRNJhsXe/Cr5k8Nhb21y3MXmmhwvje3d5PgSdk8lI11sLpJ8HfYtIslNbDaSKNZHkhKbiSSbgbDfKZKU2EwkmQ9bZvtv+VRDYu99VK5eGtt7dHn612Lf5JnyZ/5g7TPk+s1A2O/0DLl+PmyZ7f/JJxoae/ejcvXy2O4jy7O/Fvsmz5Q/8wdrnyHXbwbCfqdnyPXzYct8u3yaKbD3PypXL4/tPrI8+2uxb/JM+TN/sPYZcv1mIOx3eoZcPx+2zDfK55gO2+VRuforsP1HlSd/NfZdnil/5l9Y9wy5fjMQ9js9Q66fD1vmG2T9JbD9HpFrvwb7BqPJU78e+zbPkj/xB2sflas3g2G/1aNy9bzYUqvJqkti+94rV34V9h1Gk6du/od9n2fI9Yr1j8i1mwGx3+sRuXZ+bLmR5dmbzZDYv9mrctVGsO91j1x3Cpu/ItdsJsB+vytyzWaz2Ww2m81ms9lsNpvNZrPZbDabzWaz2Ww2m80X8Z///H9oRA0hPdqFtgAAAABJRU5ErkJggg==',
                height: 45, width: 155, top: 0, left: 0 }];
            
                this.styles = { verticalAlign: 'middle', textAlign: 'center', fontSize: '16pt', fontWeight: 'bold',
            border: '1px solid #e0e0e0', backgroundColor: '#ffffff', color: '#000000' };
      
    
            this.stylesBorder={
                border: '1px solid #e0e0e0'
            }
          
        
   
        
        }
        componentDidMount(){
            
            //  let abc = this.state.dataset2.ApplicationNo.trim();

            this.spreadsheet.cellFormat({ fontWeight: 'bold', fontSize: '12pt',backgroundColor: '#ffffff', color: '#000000', borderBottom: '1px solid #e0e0e0' }, 'B21:J21');
            this.spreadsheet.cellFormat({ backgroundColor: '#cccccc' }, 'I3:I6');
            this.spreadsheet.cellFormat({ backgroundColor: '#cccccc' }, 'A9:F20');
            this.spreadsheet.cellFormat({ backgroundColor: '#cccccc' }, 'J9:J20 ');
            this.spreadsheet.setBorder({ border: '1px solid' }, 'A9:J21');
            this.spreadsheet.addToolbarItems('Home',[{ type: 'Separator',iconCss:'e-save e-icons' }, {
                text: 'Save',  iconCss: 'e-save e-icons',
                // You can set click handler for each new custom toolbar item
                click: () => {
                    this.getData();
                    // You can add custom formulas here.
                }
            }])
            this.spreadsheet.addToolbarItems('Home', [{ type: 'Separator' }, {
                text: 'Print',   iconCss: 'bi bi-printer',
                // You can set click handler for each new custom toolbar item
                click: () => {
                this.printData();
                    // You can add custom formulas here.
                }
            }])
    
            let protectSetting = {
                selectCells: true,
                formatCells: true,
                formatRows: true,
                formatColumns: true,
                insertLink: true
            
            }
            this.spreadsheet.protectSheet("G703", protectSetting);
            this.spreadsheet.lockCells('A2:Z2', true); 
            this.spreadsheet.lockCells('A3:F6', true); 
            this.spreadsheet.lockCells('G3:G6', true); 
            this.spreadsheet.lockCells('G9:G14', true); 
            this.spreadsheet.lockCells('G9:G14', true); 
            this.spreadsheet.lockCells('H9:H14', true); 
            this.spreadsheet.lockCells('I9:I14', true); 
            this.spreadsheet.lockCells('A9:J9', true); 
            this.spreadsheet.lockCells('A10:F20', false); 
            this.spreadsheet.lockCells('J10:J20', false); 
            this.spreadsheet.numberFormat('0.00', 'C10:J20');
            this.spreadsheet.numberFormat('$#,##0.00', 'C21:J21');
            this.spreadsheet.numberFormat('0.00%', 'H10:H21');
    
            // this.spreadsheet.numberFormat("mm-dd-yyyy", 'I4:I51');
            // this.spreadsheet.clear({type: 'Clear All', range: 'A9:J9'}); 
            // this.spreadsheet.hideRow("")
            // this.spreadsheet.setRowHeight("A9",0)
            this.spreadsheet.numberFormat('NA', 'I3:J3');
            this.spreadsheet.numberFormat("@", 'I4:I5');
            // this.spreadsheet.addDataValidation({ type: 'Date', operator: 'NotEqualTo', value1: '04/11/2019' }, 'I4:I5');
        //    this.spreadsheet.allowInsert=true;
        this.spreadsheet.numberFormat('#,##0.00', 'C10:G20');
        this.spreadsheet.numberFormat('#,##0.00', 'I10:J20');
        }

        created() {

      
            
            this.isCreated = true;
            this.spreadsheet.hideFileMenuItems(['File']);
            // this.spreadsheet.allowInsert=true;
            // this.spreadsheet.hideRibbonTabs(['Home'],false);
            this.spreadsheet.hideToolbarItems('Home', [0, 1, 2, 4, 14, 15, 21, 24]);
            this.spreadsheet.hideRibbonTabs(['Insert']);
            this.spreadsheet.hideRibbonTabs(['Formulas']);
            this.spreadsheet.hideRibbonTabs(['Data']);
            this.spreadsheet.hideRibbonTabs(['View']);
            this.spreadsheet.lockCells('A2:Z2', true); 
            this.spreadsheet.lockCells('A3:F6', true); 
            this.spreadsheet.lockCells('G3:G6', true); 
            this.spreadsheet.lockCells('G9:G14', true); 
            this.spreadsheet.lockCells('G9:G14', true); 
            this.spreadsheet.lockCells('H9:H14', true); 
            this.spreadsheet.lockCells('I9:I14', true); 
            this.spreadsheet.lockCells('A9:J9', true); 
            this.spreadsheet.cellFormat({ fontWeight: 'bold', textAlign: 'left',  backgroundColor: '#ffffff', color: '#000000', borderBottom: '1px solid #e0e0e0' }, 'A2');        
            this.spreadsheet.wrap("A3", true);
            //   this.spreadsheet.hideRow(8);
            this.spreadsheet.cellFormat({ backgroundColor: '#cccccc' }, 'I3:I6');
            const cel='A9:F20';
            this.spreadsheet.cellFormat({ backgroundColor: '#cccccc' }, cel);
            this.spreadsheet.cellFormat({ backgroundColor: '#cccccc' }, 'J9:J20 ');
            this.spreadsheet.cellFormat({ textAlign: 'center' }, 'A7:J7');
            this.spreadsheet.wrap("A7:J7", true);
            this.spreadsheet.wrap("C7:C8", true);
            this.spreadsheet.wrap("D8:E8", true);
            this.spreadsheet.setRowHeight("D8",140);
            this.spreadsheet.cellFormat({ textAlign: 'center' }, 'A8:J8');
            this.spreadsheet.cellFormat({ fontWeight: 'bold', fontSize: '12pt',backgroundColor: '#ffffff', color: '#000000', borderBottom: '1px solid #e0e0e0' }, 'B21:J21');     
            this.spreadsheet.setBorder({ border: '1px solid' }, 'A7:J7');
            this.spreadsheet.setBorder({ border: '1px solid' }, 'A3:F3');
            this.spreadsheet.setBorder({ border: '1px solid' }, 'I3:I6');
            this.spreadsheet.setBorder({ border: '1px solid' }, 'D8:E8');
            this.spreadsheet.setBorder({ border: '1px solid' }, 'A9:J9');
            this.spreadsheet.setBorder({ border: '1px solid' }, 'A10:J10');
            this.spreadsheet.setBorder({ border: '1px solid' }, 'A11:J11');
            this.spreadsheet.setBorder({ border: '1px solid' }, 'A12:J12');
            this.spreadsheet.setBorder({ border: '1px solid' }, 'A13:J13');
            this.spreadsheet.setBorder({ border: '1px solid' }, 'A9:J21');      
            // this.spreadsheet.numberFormat("mm-dd-yyyy", 'I4:I51');
            this.spreadsheet.addDataValidation({ type: 'Date', operator: 'NotEqualTo', value1: '04/11/2019' }, 'I4:I5');
            // this.spreadsheet.clear({type: 'Clear All', range: 'A9:J9'}); 
            // this.spreadsheet.delete(8, 'Row', 0); // startIndex, endIndex, Row, sheet index
            // this.spreadsheet.hideRow(8);
            this.spreadsheet.setRowHeight(1,8);
            this.spreadsheet.numberFormat('@', 'I3:J3');
            this.spreadsheet.numberFormat("@", 'I4:I5');
            this.spreadsheet.numberFormat('#,##0.00', 'C10:G20');
            this.spreadsheet.numberFormat('$#,##0.00', 'C21:J21');
            this.spreadsheet.numberFormat('$#,##0.00', 'C21:G21');
            this.spreadsheet.numberFormat('#,##0.00', 'I10:J20');
          
        }

        beforeSave(args) {

            var kkkk=this.response;
            var abcddd= this.spreadsheet.saveAsJson().then(Json => (this.response = Json)); 
            // var xlObj =document.getElementById('spreadsheet').data("ejSpreadsheet");
            
            args.customParams = { customParams: 'you can pass custom params in server side' }; // you can pass the custom params
        }
    
        loadDocumentList() {
          
        axios.get('http://localhost:28358/api/spreadsheets').then((res) => {
          console.log(res);

          if (res != null) {
            this.ExcelsList = res;
            setTimeout(() => {
                debugger;
                // console.log(res.data);
                // this.setState({fileList: res.data})
                
               
                this.props.parentCallback(res.data);
                  
                // this.setState({fileList: fileList:res[0].fileName})
            //   this.selectedFileName = res[0].fileName;
              this.loadDocumentFromUrl(res.data[0].fileName);
            }, 1000);
    
          }
        });
         }

         loadDocumentdata(fileName) {
            //  alert(fileName);
            debugger;
      
            axios.get("http://localhost:28358/api/spreadsheets/getfilebyname/" + fileName, { responsetype: 'json' }).then(res=>{
                // var abc=res;
                // console.log(abc.data);
                  this.state.dataset2=res.data;
                    console.log(res.data, "mm");

                // this.spreadsheet.sheets[0].ranges[0].dataSource = [];
                // this.setState({spData:res.data});
                })
          }

        loadDocumentFromUrl(name) {

        axios.get("http://localhost:28358/api/spreadsheets/GetFileByName/" + name).then((fileDetail) => {
        //   console.log(fileDetail,'A');
          this.fileDetail = fileDetail;
          this.state.dataset2=fileDetail;
        //   this.props.spDatas=fileDetail;
          axios.get("http://localhost:28358/Excels/"+ name , { responseType: 'json' }).then((fileData) => {
            // this.loadDocument(fileData);
            // this.props.parentCallback(fileData.data);
            console.log(fileData,'h');
            
             this.loadDocumentdata(name)
            
          //  this.spreadsheet.sheets[0].ranges[0].dataSource = this.state.dataset2; //fileData;
          });
        });
        }

        getData(){
            var usedRowIdx = this.spreadsheet.getActiveSheet().usedRange.rowIndex;
            var usedColIdx = this.spreadsheet.getActiveSheet().usedRange.colIndex;
            this.spreadsheet.getData(this.spreadsheet.getActiveSheet().name + "!"+ getRangeAddress([0, 0, usedRowIdx, usedColIdx])).then((cells)=>{
                var datas = [];
                var cols = {};
            cells.forEach((cell, key)=>{
                // var cols = {};
                console.log(  key + " : " + cell.value);
                // cols.push(key + " : " + cell.value);
                // datas.push("{"+key + ""+":"+"" + cell.value+"}")
                // datas.push(key + " : " + cell.value);
            var obj="{" + "'Cell':" +"'"+ key + "','" + "Value':'" + cell.value+"'}";
                // datas.push("{'"+key +  '"' +":"+'"'+ cell.value+"'}");
            datas.push(obj);
            })
            var abkk=datas;
            console.log(abkk);
            const json = JSON.stringify(datas);
            var selFileName=this.props.sfileName;
            axios.post('http://localhost:28358/api/spreadsheets/Save/' + selFileName, JSON.stringify(json), {
            headers: {
                'Content-Type': 'application/json',
            }
            }
            ).then(res=>{
                this.loadDocumentList();
                alert('Saved Successfully');
            // console.log(res);
            // this.setState({fileList:res.data});
            });

            });
        
            }
    
        printData(){
                var usedRowIdx = this.spreadsheet.getActiveSheet().usedRange.rowIndex;
                var usedColIdx = this.spreadsheet.getActiveSheet().usedRange.colIndex;
                this.spreadsheet.getData(this.spreadsheet.getActiveSheet().name + "!"+ getRangeAddress([0, 0, usedRowIdx, usedColIdx])).then((cells)=>{
                    var datas = [];
                    var cols = {};
                cells.forEach((cell, key)=>{
                    // var cols = {};
                    console.log(  key + " : " + cell.value);
                    // cols.push(key + " : " + cell.value);
                    // datas.push("{"+key + ""+":"+"" + cell.value+"}")
                    // datas.push(key + " : " + cell.value);
                var obj="{" + "'Cell':" +"'"+ key + "','" + "Value':'" + cell.value+"'}";
                    // datas.push("{'"+key +  '"' +":"+'"'+ cell.value+"'}");
                datas.push(obj);
                })
                var abkk=datas;
                console.log(abkk);
                const json = JSON.stringify(datas);
                var selFileName=this.props.sfileName;
                axios.post('http://localhost:28358/api/spreadsheets/Print/' + selFileName, JSON.stringify(json), {
                headers: {
                    'Content-Type': 'application/json',
                }
                }
                ).then(res=>{
                var NewURL="http://localhost:28358/Excels/"+ res.data.fileName;
                window.open(NewURL);
                // console.log(res);
                // this.setState({fileList:res.data});
            });
            
                });
                
        }
    
      
        componentDidUpdate(){
            // this.spreadsheet.sheets[0].ranges[0].dataSource = this.state.dataset2; //fileData;
            
            this.spreadsheet.isProtected=false;
            this.spreadsheet.cellFormat({ fontWeight: 'bold', fontSize: '12pt',backgroundColor: '#ffffff', color: '#000000', borderBottom: '1px solid #e0e0e0' }, 'B21:J21');
            this.spreadsheet.cellFormat({ backgroundColor: '#cccccc' }, 'I3:I6');
            this.spreadsheet.cellFormat({ backgroundColor: '#cccccc' }, 'A9:F20');
            this.spreadsheet.cellFormat({ backgroundColor: '#cccccc' }, 'J9:J20 ');
            this.spreadsheet.setBorder({ border: '1px solid' }, 'A9:J21');        
            this.spreadsheet.lockCells('A2:Z2', true); 
            this.spreadsheet.lockCells('A3:F6', true); 
            this.spreadsheet.lockCells('G3:G6', true); 
            this.spreadsheet.lockCells('G9:G14', true); 
            this.spreadsheet.lockCells('G9:G14', true); 
            this.spreadsheet.lockCells('H9:H14', true); 
            this.spreadsheet.lockCells('I9:I14', true); 
            this.spreadsheet.lockCells('A9:J9', true); 
            this.spreadsheet.lockCells('A10:F20', false);
            this.spreadsheet.lockCells('I3:I6', false); 
            this.spreadsheet.lockCells('J10:J20', false); 
   
       
            // this.spreadsheet.numberFormat('', 'I4:I5');
            this.spreadsheet.numberFormat('@', 'I3:J3');
            // this.spreadsheet.addDataValidation({ type: 'Date', operator: 'NotEqualTo', value1: '04/11/2019' }, 'I4:I5');
            this.spreadsheet.updateCell({ value: this.props.spDatas.ApplicationNo }, "I3");
            this.spreadsheet.updateCell({ value: this.props.spDatas.ApplicationDate }, "I4");
            this.spreadsheet.updateCell({ value: this.props.spDatas.PeriodTo }, "I5");
            this.spreadsheet.updateCell({ value: this.props.spDatas.ArchitechProjectNo }, "I6");
            // this.spreadsheet.clear({type: 'Clear All', range: 'A9:J9'}); 
            // this.spreadsheet.addDataValidation({ type: 'Date', operator: 'NotEqualTo', value1: '04/11/2019' }, 'I4:I5');
            // this.spreadsheet.updateCell({ value:  "44542" }, "I4");
            // this.spreadsheet.numberFormat("mm-dd-yyyy", 'I4:I5');
            // this.spreadsheet.numberFormat("dd/MM/yyyy", 'I4:I5');
            this.spreadsheet.numberFormat("@", 'I4:I5');
            
            this.spreadsheet.numberFormat('0.00', 'C10:J20');
            this.spreadsheet.numberFormat('$#,##0.00', 'C21:J21');
            this.spreadsheet.numberFormat('0.00%', 'H10:H21');
            this.spreadsheet.numberFormat('$#,##0.00', 'C21:G21');
            this.spreadsheet.numberFormat('#,##0.00', 'C10:G20');
            this.spreadsheet.numberFormat('#,##0.00', 'I10:J20');
       
        }

        beforeSave(args) {
            args.customParams = { customParams: 'you can pass custom params in server side' }; // you can pass the custom params
        }

        onSaveClick() {
            this.spreadsheet.saveAsJson().then(Json => (this.response = Json));
          }

        render() {
            // var a = new String(this.props.spDatas.ApplicationNo);
           // var abc= "";//this.props.spDatas.ApplicationNo;
            // let abc:string= this.props.spDatas.ApplicationNo;
           // var abc="g555" //this.props.spDatas.ArchitechProjectNo;
          
               
            //if(abc !== undefined)
            //{
              //  this.state.AppNo=this.props.spDatas.ArchitechProjectNo; //String.stringify(this.props.spDatas.ArchitechProjectNo);
                //abc=this.state.AppNo;
                // abc=`${abc}`;
             // abc=JSON.stringify((abc).toString());
                // console.log(`${abc}`);
                // var abc=(abc).toString();
                // var myString= (abc).toString();
                // console.log(myString.trim());
                // // abc=abc.toString();
            //}
            
            console.log(this.props.spDatas.ApplicationNo,"kk");
            // const abc=this.props.spDatas.ArchitechProjectNo;
            const data = this.props.spDatas;
            this.state.dataset2=this.props.spDatas;
            console.log(this.props.spDatas, "aaaaa", data, this.spreadsheet, this.isCreated );
            // this.spreadsheet.sheets[0].ranges[0].dataSource = data;
            
            if(this.isCreated){
            
                //this.spreadsheet.sheets[0].ranges[0].da
                // this.spreadsheet.sheets[0].ranges[0].dataSource = data;
                //  this.spreadsheet.cellFormat({ backgroundColor: '#cccccc' }, 'A10:J10');
                this.spreadsheet.sheets[0].ranges[0].dataSource=this.props.spDatas.data;
    
                // this.spreadsheet.updateCell({ value: 'Custom Value' }, "A3");
            // this.spreadsheet.sheets[0].ranges[0].dataSource = data;
                //   this.spreadsheet.cellFormat({ backgroundColor: '#cccccc' }, 'A10:J10');
                // this.isCreated=false;

                this.spreadsheet.setBorder({ border: '1px solid' }, 'A7:J7');
                this.spreadsheet.setBorder({ border: '1px solid' }, 'A3:F3');
                this.spreadsheet.setBorder({ border: '1px solid' }, 'I3:I6');
                this.spreadsheet.setBorder({ border: '1px solid' }, 'D8:E8');
                this.spreadsheet.setBorder({ border: '1px solid' }, 'A9:J9');
                this.spreadsheet.setBorder({ border: '1px solid' }, 'A10:J10');
                this.spreadsheet.setBorder({ border: '1px solid' }, 'A11:J11');
                this.spreadsheet.setBorder({ border: '1px solid' }, 'A12:J12');
                this.spreadsheet.setBorder({ border: '1px solid' }, 'A13:J13');
                this.spreadsheet.setBorder({ border: '1px solid' }, 'A21:J21');
                this.spreadsheet.lockCells('A9:J9', false); // to lock the A1:Z1 cells
                // this.spreadsheet.hideRow(8);
                // this.spreadsheet.setRowHeight('A8',0);
                // this.spreadsheet.numberFormat("dd/MM/yyyy", 'I4:I5');
                this.spreadsheet.numberFormat("@", 'I4:I5');
       
            }
         
            return  (<> 
   
                    {/* <h1>{JSON.stringify(this.props.spDatas)}</h1> */}
            {/* <Button  onClick={handleSubmit}>Save</Button> */}
            {/* {<h5>{abc}</h5>} */}
            {/* <div>
                <button className="e-btn" onClick={this.getData.bind(this)}>Save</button>    <button className="e-btn" onClick={this.printData.bind(this)}>Print</button>   
                <span style={{fontWeight: "bold",textAlign:"center", paddingLeft:"35%"}}>{this.props.sfileName}</span>
            </div>  */}
          {/* <button cssClass='e-primary' onClick={this.onSaveClick.bind(this)} >Save</button> */}
            <SpreadsheetComponent id='spreadsheet' width={'100%'} showSheetTabs={false}  showFormulaBar={false} ref={(ssObj) => { this.spreadsheet = ssObj; }} customHeight={true} height={'100%'} allowResizing={true} created={this.created.bind(this)}  >
                            <SheetsDirective >
                                <SheetDirective name='G703' >
                                <RowsDirective >
                                    
                                <RowDirective height={50} customHeight={true}  >
                                    <CellsDirective >
                                        <CellDirective image={this.image1}  colSpan={10} value='AIA® Document G703™ – 1992'  style={this.styles} ></CellDirective>                                     
                                    </CellsDirective>
                                </RowDirective>
                                <RowDirective customHeight={true}>
                                    <CellsDirective>
                                        <CellDirective colSpan={10}
                                        value='Continuation Sheet' 
                                        ></CellDirective>                                     
                                    </CellsDirective>
                                </RowDirective>

                                <RowDirective  customHeight={true} >
                                <CellsDirective>
                                        <CellDirective rowSpan={4} colSpan={6} value='AIA Document, G702TM–1992, Application and Certification for Payment, or G736TM–2009, Project Application and Project Certificate for Payment, Construction Manager as Adviser Edition,                                                containing Contractors signed certification is 
                                                    attached. Use Column I on Contracts where variable retainage for line items may apply.'   >
                                            </CellDirective>     
                                            <CellDirective index={6} colSpan={2} 
                                            value='APPLICATION NO:'   >
                                            </CellDirective>    
                                            <CellDirective index={8} colSpan={2} 
                                               value = "">
                                            </CellDirective>                                
                                    </CellsDirective>
                                </RowDirective>

                                <RowDirective  customHeight={true}>
                                    <CellsDirective>
                                            
                                            <CellDirective index={6} colSpan={2} 
                                            value='APPLICATION DATE:'   >
                                            </CellDirective>    
                                            <CellDirective index={8} colSpan={2} 
                                            value="" type="datetime" format='dd/MM/yyyy'  >
                                            </CellDirective>                                
                                    </CellsDirective>
                                </RowDirective>

                                <RowDirective  customHeight={true}>
                                    <CellsDirective>
                                        
                                            <CellDirective index={6} colSpan={2} 
                                            value='PERIOD TO:'   >
                                            </CellDirective>    
                                            <CellDirective index={8} colSpan={2} 
                                             value="" >
                                            </CellDirective>                                
                                    </CellsDirective>
                                </RowDirective>

                                <RowDirective  >
                                    <CellsDirective>
                                        
                                            <CellDirective index={6} colSpan={2} 
                                            value='ARCHITECTS PROJECT NO:'   >
                                            </CellDirective>    
                                            <CellDirective index={8} colSpan={2} 
                                            value=""  >
                                            </CellDirective>                                
                                    </CellsDirective>
                                </RowDirective>
                                        
                                {/* <RowDirective customHeight={true}>
                                    <CellsDirective>
                                        <CellDirective  value='A'>
                                            </CellDirective>   
                                            <CellDirective  value='B'>
                                            </CellDirective>     
                                            <CellDirective  value='C'>
                                            </CellDirective>    
                                            <CellDirective  value='D'>
                                            </CellDirective>   
                                            <CellDirective  value='E'>
                                            </CellDirective>     
                                            <CellDirective  value='F'>
                                            </CellDirective>   
                                            <CellDirective  value='G' index={6} colSpan={2} >
                                            </CellDirective> 
                                            <CellDirective  value=''>
                                            </CellDirective> 
                                            <CellDirective  value='H'>
                                            </CellDirective> 
                                            <CellDirective  value='I'>
                                            </CellDirective> 
                                    </CellsDirective>                              
                                </RowDirective> */}

                                
                                <RowDirective customHeight={true}>
                                    <CellsDirective>
                                        <CellDirective  rowSpan={2}  value='ITEM No. '>
                                            </CellDirective>   
                                            <CellDirective  rowSpan={2}  value='DESCRIPTION OF WORK'>
                                            </CellDirective>   
                                            <CellDirective  rowSpan={2}  value='SCHEDULED VALUE'>
                                            </CellDirective>   
                                            <CellDirective   colSpan={2} value='WORK COMPLETED'>
                                            </CellDirective>  
                                            <CellDirective   rowSpan={2}  index={5} value='MATERIALS PRESENTLY STORED
                                                (NOT IN D OR E)'>
                                            </CellDirective>
                                            <CellDirective   rowSpan={2}  index={6} value='TOTAL COMPLETED AND STORED TO DATE
                                                (D + E + F)'>
                                            </CellDirective>
                                            <CellDirective   rowSpan={2}  index={7} value='% 
                                            (G ÷ C)'>
                                            </CellDirective>

                                            <CellDirective   rowSpan={2}  index={8} value='BALANCE TO FINISH
    (C - G)'>
                                            </CellDirective>
                                            <CellDirective   rowSpan={2}  index={7} value='RETAINAGE
    (IF VARIABLE RATE)'>
                                            </CellDirective>
                                        
                                    </CellsDirective>
                                </RowDirective>
                                <RowDirective customHeight={true}  height={60}>
                                    <CellsDirective>                                 
                                            
                                            <CellDirective  index={3}  value='FROM PREVIOUS APPLICATION
                                                                            (D + E)'>
                                            </CellDirective>  
                                            <CellDirective  index={4}  value='THIS PERIOD'>
                                            </CellDirective> 
                                        
                                    </CellsDirective>
                                </RowDirective>
                            
                                      
                                      
                                
                                    {/* <RowDirective index={13} >
                                    <CellsDirective>
                                        <CellDirective index={1} value={'Grand Total:'} style={this.cellStyle}></CellDirective>
                                        <CellDirective formula={'=SUM(C10:C13)'} ></CellDirective>
                                        <CellDirective formula={'=SUM(D10:D13)'} ></CellDirective>
                                        <CellDirective formula={'=SUM(E10:E13)'} ></CellDirective>
                                        <CellDirective formula={'=SUM(F10:F13)'} ></CellDirective>
                                        <CellDirective formula={'=SUM(G10:G13)'} ></CellDirective>
                                        <CellDirective formula={'=IF(C10=0,0,(G10/C10))'} ></CellDirective>
                                        <CellDirective formula={'=SUM(I10:I13)'} ></CellDirective>
                                        <CellDirective formula={'=SUM(J10:J13)'} ></CellDirective>
                                    </CellsDirective>
                                </RowDirective>  */}
                       
                            
                            </RowsDirective  >
                                    <RangesDirective showGridLines={true} style={ this.stylesBorder}  showHeaders={false} >
                                        <RangeDirective dataSource={this.state.dataset2.data} startCell={"A9"}  style={ this.stylesBorder}  ></RangeDirective>
                                  
                                    </RangesDirective>

                                 
                                  
                                    <ColumnsDirective protectSetting={false} style={ this.stylesBorder} >
                                        <ColumnDirective width={50}></ColumnDirective>
                                        <ColumnDirective width={150}    ></ColumnDirective>
                                        <ColumnDirective width={100}  ></ColumnDirective>
                                        <ColumnDirective width={130} ></ColumnDirective>
                                        <ColumnDirective width={120} ></ColumnDirective>
                                        <ColumnDirective width={120} ></ColumnDirective>
                                        <ColumnDirective width={120} ></ColumnDirective>
                                        <ColumnDirective width={120} ></ColumnDirective>
                                        <ColumnDirective width={120} ></ColumnDirective>
                                        <ColumnDirective width={120} ></ColumnDirective>
                                    </ColumnsDirective>
                                </SheetDirective>
                            </SheetsDirective>
                        </SpreadsheetComponent>
                
                        {/* <button className="e-btn" onClick={this.updateCollection.bind(this)}>
            Clear Data Source
            </button> */}
            </>
            
            );
            
        }
}
export default SpreadsheetSN;