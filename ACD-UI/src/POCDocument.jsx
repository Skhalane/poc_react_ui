import * as React from 'react';
// import * as ReactDOM from 'react-dom';

import { DocumentEditorContainerComponent, Toolbar } from '@syncfusion/ej2-react-documenteditor';
import $ from 'jquery';
import axios from 'axios';
import logo from './assets/img/MainLogo.svg';
import logo_white from './assets/img/MainLogo_white.svg';
import 'bootstrap/dist/css/bootstrap.min.css';


DocumentEditorContainerComponent.Inject(Toolbar);

class POCDocument extends React.Component {

  constructor(props) {
    super(props)

    this.state = {
      documentList: [], revList: [], filedetails: "", storeFileName: "", count: "", isDocumentMenuOpen: false, isRightMenuOpen: false,
      documentArray: [], selectedRow: -1,
    };

  }

  toggledocumentMenu = () => {
    this.setState({ isDocumentMenuOpen: !this.state.isDocumentMenuOpen })
  }
  toggleRightMenu = () => {
    this.setState({ isRightMenuOpen: !this.state.isRightMenuOpen })
  }



  componentDidMount = () => {
   // $('.e-tbar-btn-text').css("visibility", "hidden");
    $('.e-tbar-btn-text').css("display", "none");
  
    this.Toolfunction();
    this.loadDocumentList();
    this.dragandDropfunction();
  }

  componentWillUnmount() {
    document.removeEventListener("dragstart", this.onDragStartHandler);

    document.removeEventListener("dragover", this.ondragoverHandler);
    
    document.removeEventListener("drop",this.ondropHandler);
  }
  loadDocumentList = () => {
    fetch('http://localhost:28358/api/documents').then(res => res.json()).then(res => {
      //debugger;
      this.setState({ documentList: res });
      this.modityArray();


      this.loadDocumentFromUrl(res[3].fileName);
    });


    //  this.loadDocumentFromUrl(res[0].fileName);
    this.container.serviceUrl = 'https://ej2services.syncfusion.com/production/web-services/api/documenteditor/';
  }
  modityArray = () => {
    // debugger
    var array = this.state.documentList;

    for (var i = 0; i < array.length; i++) {

      if (array[i].version == null) {
        array[i].version = [];
      }
    }
    // console.log(array);
    this.setState({ documentArray: array });
  }
  loadDocumentFromUrl = (filename) => {
   // debugger;
    //$("#vId").css("color", "");
    this.setState({ storeFileName: filename });
    localStorage.setItem("Filenamekey", filename);

    axios.get("http://localhost:28358/api/documents/GetFileByName/" + filename, { responseType: 'json' }).then((fileDetail) => {


      axios.get("http://localhost:28358/Docs/" + fileDetail.data.fileName, { responseType: 'text' }).then((htmlData) => {
        // debugger;
        this.loadDocument(htmlData);

      });
    });
  }

  loadDocument = (htmlData) => {
    // debugger;
    var editorDiv = document.querySelector("#container #container_editor_viewerContainer");
    editorDiv.innerHTML = htmlData.data;
    $("a").css("color", "black");
    this.append();
  }

  append = () => {
    // debugger
    var containerid = document.getElementById('container_editor');
    var elemText = document.getElementById('container_editor_viewerContainer');


    // elemText.setAttribute('contenteditable', true);
    var element = document.getElementById('container_editor_viewerContainer');
    element.style.removeProperty("overflow");

    containerid.style.setProperty("overflow", "auto");
    containerid.style.setProperty("padding-top", "40px");
    containerid.style.setProperty("padding-left", "40px");

    if (this.state.storeFileName == "B104-2017.docx" || this.state.storeFileName == "B104-2007.docx") {
      elemText.setAttribute('contenteditable', true)
    } else {
      elemText.setAttribute('contenteditable', false)
    }

    //debugger;
    var allbookmarks = $("[name^=bm_]");
    for (let i = 3; i < allbookmarks.length; i++) {



      if (allbookmarks[i + 1] != undefined) {
        if (allbookmarks[i].getAttribute("name") === allbookmarks[i + 1].getAttribute("name")) {

        }
        else {

          var name = allbookmarks[i].getAttribute("name");
          name = name.replace("bm_", "")


          var innertext = allbookmarks[i].innerText.trim();
          if (innertext == "" || innertext == "[" + name + "]") {
            allbookmarks[i].innerHTML = this.getChoiceHtml(name);


          }

          // $('.bookmarkclass').attr("id",name);
        }
      }
      else {
        break;
      }



    }

    var bm_ContractDateDayWordsRankedHtml = "<select style='color: blue;' id='ContractDateDayWordsRanked'>&nbsp"
    for (let i = 1; i <= 30; i++) {
      bm_ContractDateDayWordsRankedHtml = bm_ContractDateDayWordsRankedHtml + "<option value=" + i + ">" + i + "</option>"
    }
    bm_ContractDateDayWordsRankedHtml = bm_ContractDateDayWordsRankedHtml + "</select>&nbsp";
    var bm_ContractDateDayWordsRanked = $("[name='bm_ContractDateDayWordsRanked']");
    if (bm_ContractDateDayWordsRanked.length > 0) {
      bm_ContractDateDayWordsRanked[0].innerHTML = bm_ContractDateDayWordsRankedHtml;
      //bm_ContractDateDayWordsRanked[0].setAttribute('name', "ContractDateDayWordsRanked_Updated");
    } else {
      var bm_AgreementDateDayWordsRanked = $("[name='bm_AgreementDateDayWordsRanked']");
      bm_AgreementDateDayWordsRanked[0].innerHTML = bm_ContractDateDayWordsRankedHtml;
    }
    var bm_ContractDateMonthWordshtml = `<select style='color: blue;' id='ContractDateMonthWords'>
   <option value='1'>Janaury</option>
   <option value='2'>February</option>
   <option value='3'>March</option>
   <option value='4'>April</option>
   <option value='5'>May</option>
   <option value='6'>June</option>
   <option value='7'>July</option>
   <option value='8'>August</option>
   <option value='9'>September</option>
   <option value='10'>October</option>
   <option value='11'>November</option>
   <option value='12'>December</option> </select>`;

    bm_ContractDateMonthWordshtml = bm_ContractDateMonthWordshtml + "</select>&nbsp";
    var bm_ContractDateMonthWords = $("[name='bm_ContractDateMonthWords']");
    if (bm_ContractDateMonthWords.length > 0) {
      bm_ContractDateMonthWords[0].innerHTML = bm_ContractDateMonthWordshtml;
    } else {
      var bm_AgreementDateMonthWords = $("[name='bm_AgreementDateMonthWords']");
      bm_AgreementDateMonthWords[0].innerHTML = bm_ContractDateMonthWordshtml;
    }

    //bm_ContractDateMonthWords[0].setAttribute('name', "ContractDateMonthWords_Updated");

    var bm_ContractDateYearWordshtml = "&nbsp<select style='color: blue;' id='ContractDateYearWords'>"
    for (let i = 1; i <= 30; i++) {
      var year = (2000 + i);
      bm_ContractDateYearWordshtml = bm_ContractDateYearWordshtml + "<option value=" + year + ">" + year + "</option>"
    }
    bm_ContractDateYearWordshtml = bm_ContractDateYearWordshtml + "</select>";
    var bm_ContractDateYearWords = $("[name='bm_ContractDateYearWords']");
    if (bm_ContractDateYearWords.length > 0) {
      bm_ContractDateYearWords[0].innerHTML = bm_ContractDateYearWordshtml;
    } else {
      var bm_AgreementDateYearWords = $("[name='bm_AgreementDateYearWords']");
      bm_AgreementDateYearWords[0].innerHTML = bm_ContractDateYearWordshtml;
    }
    this.appendChangeEventOnDropdown();





  }


  getChoiceHtml = (innerText) => {
    return "<span id='" + innerText + "' style='color: blue;' class='bookmarkclass' contentEditable='true'>[" + innerText + "]</span>";

  }


  appendChangeEventOnDropdown = () => {
    // debugger
    $("#container_editor_viewerContainer").on('change', 'select', function () {
      var value = $(this).val();
      $(this).find('option[selected="selected"]').removeAttr('selected');
      $(this).val(value)
        .find("option[value=" + value + "]").attr('selected', true);
    });

  }

  getChoiceHtmlWithoutSqureBracket = (innerText) => {
    // debugger
    return "<span style='color: blue;' contentEditable='true'>[" + innerText + "]</span>";
  }

  onPrintDocument = () => {
    // debugger;

    // var selFileName = localStorage.getItem("Filenamekey");
    var selFileName = this.state.storeFileName;

    var editorDiv = document.querySelector("#container #container_editor_viewerContainer");

    axios.post('http://localhost:28358/api/documents/Print/' + selFileName, JSON.stringify(editorDiv.innerHTML), {
      headers: {
        'Content-Type': 'application/json',

      },
      responseType: 'json'
    }
    ).then((res) => {

      //  debugger;
      var NewURL = "http://localhost:28358/Docs/" + res.data.fileName;

      window.open(NewURL);

      alert('Printed Successfully');

    });
  }



  savefunction = () => {
    // debugger;
    //  var selFileName = localStorage.getItem("Filenamekey");
    var selFileName = this.state.storeFileName;
    var editorDiv = document.querySelector("#container #container_editor_viewerContainer");

    axios.post('http://localhost:28358/api/documents/Save/' + selFileName, JSON.stringify(editorDiv.innerHTML), {
      headers: {
        'Content-Type': 'application/json',
      }
    }
    ).then((res) => {

      this.loadDocumentList()
      alert('Saved Successfully');
    });

  }


  loadVersionFile = (fileDetail, filename) => {
    // debugger
   // $("#vId").css("color", "red");
    this.setState({ storeFileName: filename });
    var header = fileDetail.replace(".docx", "") + "/" + filename;

    axios.get("http://localhost:28358/Docs/" + header).then((htmlData) => {

      debugger
      //  this.loadDocument(htmlData)
      var editorDiv = document.querySelector("#container #container_editor_viewerContainer");
      editorDiv.innerHTML = htmlData.data;
      //  this.append();


    });
  }


  Toolfunction = () => {

   

    var Div = document.getElementsByClassName("e-de-toolbar-btn-start");
    var Saveandprintbutton = '<span id="save" class="fa fa-save e-btn-icon e-icons e-icon-left" title="save"></span><span id="print" title="print" class="e-btn-icon e-icons e-icon-left fa fa-print"></span>';
    Div[0].innerHTML = Saveandprintbutton;
    document.getElementById("save").onclick = this.savefunction;
    document.getElementById("print").onclick = this.onPrintDocument;
   

 

  }

  onDragStartHandler = (event) => {
    event.dataTransfer.setData("Text", event.target.id);
  }
  ondragoverHandler = (event) => {
    event.preventDefault();
  }

  ondropHandler = (event) => {
    event.preventDefault();
    var data = event.dataTransfer.getData("Text");

    var itm = document.getElementById(data);
  // itm.attr("color","blue");
    var cln = itm.cloneNode(true);
    console.log(data, cln);
    const a = event.target
    a.textContent = '';
    event.target.appendChild(cln);


  }


  dragandDropfunction = () => {

    document.addEventListener("dragstart", this.onDragStartHandler);

    document.addEventListener("dragover", this.ondragoverHandler);
    
    document.addEventListener("drop",this.ondropHandler);

  }



  render() {

    var { documentArray } = this.state;

    const MainHeaderstyle = {
      display: "flex",
      backgroundColor: "#007bff",
      padding: "10px 20px",
      boxShadow: "0 2px 2px -2px rgba(0,0,0,.2)",
      alignItems: "center",
      height: "50px"

    };

    const vfilestyle = {
      cursor: "pointer",

      fontSize: "13px",
      paddingTop: "5px",
      marginLeft: "10px",
    }
    const leftstyle = {
      //  display:"none",
      backgroundColor: "#eee",
      padding: "10px",
      height: "100%",
      flex: "0 0 280px"
    };

    const trtag = {
      display: "flex",
      padding: "10px 5px 5px",
      flexWrap: "wrap",
      cursor: "pointer",
      fontWeight: "600",
      fontSize: "14px",

    };
    const headername = {
      fontWeight: "bold",
      display: "flex",
      // padding: "2rem",
      flexWrap: "wrap",
      fontSize: "15px",
      paddingTop: "10px"
    }

    const tablestyle = {
      display: "block",
      // marginLeft: "15px"
    }
    const rightIcon={
      fontSize: "16px",
    }

    return (<React.Fragment>
      <div className="Main-body-container">
        <div class="Main-header" style={MainHeaderstyle}>
          <div class="main-logo">
            <img src={logo_white} className="App-logo" alt="logo" />
          </div>
          <div className="right-side-nav">
            <div className="menu-item-group">
              <i class="far fa-question-circle menu-item"></i>
              <i class="far fa-bell menu-item"></i>
            </div>
            <div className="user_profile">
              <div className="user_avtar">JD</div>
              <i class="fas fa-sort-down"></i>
            </div>
          </div>




        </div>

        <div class="row  main-doc-body-wrapper" >
          <button className="documentToggleBtn btn" onClick={this.toggledocumentMenu} type="button" >
            {(!this.state.isDocumentMenuOpen) &&
              <i class="fas fa-arrow-circle-right"></i>
            }
            {(this.state.isDocumentMenuOpen) &&
              <i class="fas fa-arrow-circle-left"></i>
            }
          </button>
          {
            (this.state.isDocumentMenuOpen) &&
            <div class="sidemenu_document_list" style={leftstyle}>
              <table style={tablestyle}>
                <thead><tr class="" style={headername}><span className="doument_list_header">Scarmon Residence Remodel</span> </tr></thead>
                <tbody>
                  <tr>

                    {
                      documentArray.map(({ fileName, version }, i) => (
                        <div key={i}>
                          <div>
                            <span onClick={() => this.loadDocumentFromUrl(fileName)} style={trtag} className="doc_file_name"><span className="title-icon document_icon"><i class="far fa-file-alt"></i></span> <span>{fileName.replace(".docx","")}</span></span>
                            {version.map((vfile, j) => <div className="html_file_name" onClick={() => this.loadVersionFile(fileName, vfile)} style={vfilestyle} key={j}>
                              <span className="user_initials user_color2" >DG</span>
                              <span className="file_name">{vfile.replace(".html","")}</span>
                              <span className="time_stamp">6/10/2021 10:23am</span>
                            </div>)}

                          </div>
                        </div>
                      ))
                    }
                  </tr>




                </tbody>
              </table>
            </div>
          }
          <div class="col-lg-12" id="documenteditor_container_body">
            {/* <span  onClick={this.collaspeTool} style={icontag} class="glyphicon glyphicon-eject"></span> */}
            <div className="doc_project_title"><strong>Project:</strong> Brookwood High school, Snellville, GA | A101 2007</div>

            <DocumentEditorContainerComponent isReadOnly={false} height={"100%"} id="container" ref={(scope) => { this.container = scope; }} style={{ 'display': 'block' }} />
          </div>

          {/* Right Side Panel    className="detailsToggleBtn btn" */}
          <button className="detailsToggleBtn" onClick={this.toggleRightMenu} type="button" >        
              <i class="fas fa-indent " style={rightIcon}></i>   
          </button>
          {
            (this.state.isRightMenuOpen) &&
            <div class="sidemenu_detail_list" style={leftstyle}>
          <button className="detailsCloseeBtn" onClick={this.toggleRightMenu} type="button" >           
              <i class="far fa-times-circle"></i>            
          </button>
              <table style={tablestyle}>
                <thead><tr style={headername}><span className="doument_list_header">Global Terms</span> </tr></thead>
                <tbody>
                  <tr>
                    <div>
                      <span style={trtag} className="right_menu_title"><span>Project Name</span></span>
                      <div className="project_value">
                        <i class="fas fa-arrows-alt user_initials user_color2"></i> 
                        <span class="dragValue" draggable="true" id="ProjectNameId">Aryeh Siegel Architect</span>
                                      </div>

                    </div>
                  </tr>
                  <tr>
                    <div>
                      <span style={trtag} className="right_menu_title"><span>Project Value</span></span>
                      <div className="project_value">
                        <i class="fas fa-arrows-alt user_initials user_color2"></i>
                        <span class="dragValue" draggable="true" id="ProjectValueId">$ 10 Million</span>
                                      </div>

                    </div>
                  </tr>
                  <tr>
                    <div>
                      <span style={trtag} className="right_menu_title"><span>Project Address</span></span>
                      <div className="project_value">
                        <i class="fas fa-arrows-alt user_initials user_color2"></i> 
                         <span class="dragValue" draggable="true" id="AddressId"> 25, East Main Street, Beacon, NY - 12508</span>
                                      </div>

                    </div>
                  </tr>
                  <tr>
                    <div>
                      <span style={trtag} className="right_menu_title"><span>Architect Name</span></span>
                      <div className="project_value">
                        <i class="fas fa-arrows-alt user_initials user_color2"></i>
                        <span class="dragValue" draggable="true" id="ArchitectNameId">John Doe</span>
                                      </div>

                    </div>
                  </tr>
                  <tr>
                    <div>
                      <span style={trtag} className="right_menu_title"><span>Architect Address</span></span>
                      <div className="project_value">
                        <i class="fas fa-arrows-alt user_initials user_color2"></i>
                        <span class="dragValue" draggable="true" id="ArchitectAddressId"> 23-28, Creek Drive, Beacon, NY - 12508</span>
                                      </div>

                    </div>
                  </tr>
                  <tr>
                    <div>
                      <span style={trtag} className="right_menu_title"><span>Owner Name</span></span>
                      <div className="project_value">
                        <i class="fas fa-arrows-alt user_initials user_color2"></i>
                        <span className="file_name">
                         <span className="file_name"><span class="dragValue" draggable="true" id="OwnerId">Matt Johnson</span></span></span>
                      </div>

                    </div>
                  </tr>
                  <tr>
                    <div>
                      <span style={trtag} className="right_menu_title"><span>Owner Address</span></span>
                      <div className="project_value">
                        <i class="fas fa-arrows-alt user_initials user_color2"></i> 
                        <span class="dragValue"  draggable="true" id="OwnerAddessId">25, East Main Street, Beacon, NY - 12508</span>
                      
                                      </div>

                    </div>
                  </tr>
                </tbody>
              </table>
              {/* <div class="col-lg-3" id="id1">

<label>Owner Name: </label><input type="text" id="myInput"></input>
<span>&nbsp;</span>

<button id="button1" type="button" value="Click!" onClick={() => this.Ownerfunction()} style={{ 'marginleft': '2px;' }}>Add</button>

<span>&nbsp;</span><br />
 <span draggable="true" id="OwnerId">{this.state.OwnerNamevalue}</span><br /> 




<label>Project Name: </label><input type="text" id="ProjectNameInput"></input>
<span>&nbsp;</span>

<button id="button2" type="button" onClick={() => this.ProjectNamefunction()} style={{ 'marginleft': '2px;' }}>Add</button>

<span>&nbsp;</span><br />
<span draggable="true" id="ProjectNameId">{this.state.ProjectNamevalue}</span>

</div> */}



            </div>
          }

        </div>
        <div className={"Main-footer"}>
          © 2021 AIA® Document. All rights reserved
           </div >
      </div>

    </React.Fragment>);
  }
}

export default POCDocument;
